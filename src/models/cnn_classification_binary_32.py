import numpy as np

import torch
from torch import nn, optim, from_numpy
import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader
import pytorch_lightning as pl
from torchmetrics import Accuracy

class CNN(pl.LightningModule):

    def __init__(
        self, batch_size = 128, num_workers = 0, weight_decay = 0.001, learning_rate = 0.001
    ):
        super(CNN, self).__init__()

        # Training
        self.batch_size = batch_size
        self.num_workers = num_workers
        self.weight_decay = weight_decay
        self.learning_rate = learning_rate

        # Model architecture
        self.conv1 = nn.Conv1d(4, 32, 15)
        self.conv1_bn = nn.BatchNorm1d(32)
        self.fc1 = nn.Linear(736, 256)
        self.fc1_bn = nn.BatchNorm1d(256)
        self.fc2 = nn.Linear(256, 128)
        self.fc2_bn = nn.BatchNorm1d(128) 
        self.fc3 = nn.Linear(128, 2)
        self.relu1 = nn.ReLU()
        self.relu2 = nn.ReLU()
        self.relu3 = nn.ReLU()
        

    def forward(self, x):

        def num_flat_features(x):
            size = x.size()[1:]  
            num_features = 1
            for s in size:
                num_features *= s
            return num_features
        
        x = F.pad(x, (1,1,0,0), 'constant', 0)
        x = F.max_pool1d(self.relu1(self.conv1_bn(self.conv1(x))), 10)
        x = x.view(-1, num_flat_features(x))
        x = self.relu2(self.fc1_bn(self.fc1(x)))
        x = self.relu3(self.fc2_bn(self.fc2(x)))
        x = self.fc3(x)
        return x

    def training_step(self, batch, batch_idx):
        labels, inputs = batch
        outputs = self(inputs.float())
        loss = nn.CrossEntropyLoss()
        J = loss(outputs, labels.long()) 
        _,preds=torch.max(outputs,1)
        accuracy = Accuracy(task="binary")
        acc = accuracy(preds, labels)
        self.log('acc', acc, prog_bar=True)
        self.log('train_loss', J)
        return {'loss': J, 'acc': acc}

    def validation_step(self, batch, batch_idx):
        labels, inputs = batch
        outputs = self(inputs.float())
        loss = nn.CrossEntropyLoss()
        val_loss = loss(outputs, labels.long()) 
        self.log('val_loss', val_loss, prog_bar=True)
        _,preds=torch.max(outputs,1)
        accuracy = Accuracy(task="binary")
        val_acc = accuracy(preds, labels)
        self.log('val_acc', val_acc, prog_bar=True)
        return {"val_loss": val_loss, 'val_acc': val_acc}

    def test_step(self, batch, batch_idx):
        labels, inputs = batch
        outputs = self(inputs.double())#.view(-1)
        loss = nn.CrossEntropyLoss()
        test_loss = loss(outputs, labels.long()) 
        return {"test_loss": test_loss}

    def configure_optimizers(self):
        optimizer = optim.Adam(self.parameters(), lr=(self.lr or self.learning_rate), weight_decay= self.weight_decay)
        return optimizer

    def train_dataloader(self, train_data, train_label):

        class GenomicDataset(Dataset):
            def __init__(self, data, labels):
                self.samples = labels, data

            def __len__(self):
                return len(self.samples[1])

            def __getitem__(self, idx):
                return self.samples[0][idx], self.samples[1][idx]

        trainset = GenomicDataset(train_data, train_label)
        train_loader = DataLoader(trainset, batch_size = self.batch_size, num_workers = self.num_workers, shuffle=True)

        return train_loader
    
    def val_dataloader(self, val_data, val_label):

        class GenomicDataset(Dataset):
            def __init__(self, data, labels):
                self.samples = labels, data

            def __len__(self):
                return len(self.samples[1])

            def __getitem__(self, idx):
                return self.samples[0][idx], self.samples[1][idx]

        valset = GenomicDataset(val_data, val_label)
        val_loader = DataLoader(valset, batch_size=self.batch_size, num_workers= self.num_workers, shuffle=True)

        return val_loader

    def test_dataloader(self, test_path):
        
        self.test_path = test_path
        test_data = from_numpy(np.load(test_path + 'test_data.npy').astype(float))
        test_labels = from_numpy(np.load(test_path + 'test_label.npy').astype(float))

        class GenomicTestDataset(Dataset):
            def __init__(self, data, labels):
                self.samples = labels, data

            def __len__(self):
                return len(self.samples[1])

            def __getitem__(self, idx):
                return self.samples[0][idx], self.samples[1][idx]

        testset = GenomicTestDataset(test_data, test_labels)
        test_loader = DataLoader(testset, batch_size = self.batch_size, num_workers = self.num_workers, shuffle=True)
        return test_loader