import numpy as np
import pandas as pd
import itertools

from sklearn.metrics import auc, precision_recall_curve
import matplotlib.pyplot as plt
import seaborn as sns
import torch

from misc import load_model

######################################
#### PREDICTIVE MODEL PERFORMANCE ####
######################################

##### CLASSIFICATION #####
def pred_df_neg(path, dataset, seeds, size, subexp='w'):
    ''' Create a dataframe with predictions and probabilities for negative sequence experiments

    Arguments:
    ----------
    path: Path
        Path to the main directory
    dataset: str
        Name of the dataset
    seeds: int[]
        List of seeds used for training
    size: int
        Size of the model (large or small)
    subexp: str
        Subexperiment used for testdata (w default since it also contains subsets)
    
    Output:
    -------
    pd.DataFrame
        Dataframe with predictions and probabilities for each sequence
    '''
    data_path = path / 'Classification' / 'negative_sequences' /dataset / 'data' / 'dataset'
    testdata = torch.from_numpy(np.load(data_path / str('data_test_'+subexp+'.npy'))).double()

    dict_df = {'Seed':[], 'w_pred':[], 'wo_pred':[], 'w_prob':[], 'wo_prob':[], 'label':[]}
    for seed in seeds:
        dict_df['Seed'].extend([seed]*testdata.shape[0])
        label = np.load(data_path / 'label_test.npy')
        dict_df['label'].extend(label)
        for exp in ['w', 'wo']:
            model = load_model(path, 'neg', dataset, exp, size, seed)
            model.eval() 
            with torch.no_grad():
                outputs = model(testdata)
                prob = torch.sigmoid(outputs)
                _,pred=torch.max(outputs,1) #.numpy() #.flatten().tolist()
            dict_df[exp+'_pred'].extend(pred.numpy())
            dict_df[exp+'_prob'].extend(prob[:,1].numpy())
            
    return pd.DataFrame(dict_df) 

def accuracy_motifs(preds_w, preds_wo, labels, motif_idx):
    ''' Calculate accuracy for each motif subset
    
    Arguments:
    ----------
    preds_w: int[]
        Predictions for the model with motif subsets
    preds_wo: int[]
        Predictions for the model without motif subsets
    labels: int[]
        True labels
    motif_idx: dict
        Dictionary with motif indices for subsets distinguished by number of containing motifs

    Output:
    -------
    acc_dict: dict
        Dictionary with accuracy for each motif subset
    '''
    #motif_idx = {4: range(1500), 3: range(1500,1900), 2: range(1900,2500), 1: range(2500,2900), 0: range(2900, 3000)}
    acc_dict = {'nr_motifs':[], 'w':[], 'wo':[]}

    for k in motif_idx.keys():
        idx = motif_idx[k]
        nr_seqs = len(idx)
        acc_dict['nr_motifs'].extend([k])

        true_w = np.array(preds_w[idx]) == np.array(labels)[idx]
        acc_dict['w'].extend([round(len(true_w.nonzero()[0])/nr_seqs, 4)])

        true_wo = np.array(preds_wo[idx]) == np.array(labels)[idx]
        acc_dict['wo'].extend([round(len(true_wo.nonzero()[0])/nr_seqs, 4)])
        
    return acc_dict

def accuracy_motifs_df(path, datasets, seeds, motif_idx):
    ''' Wrapper for accuracy_motifs. Creates dataframe with accuracy for each motif subset

    Arguments:
    ----------
    path: str
        Path to the directory with data
    datasets: str[]
        List of datasets
    seeds: int[]
        List of seeds
    motif_idx: dict
        Dictionary with motif indices for subsets distinguished by number of containing motifs

    Output:
    -------
    dict_df: pd.DataFrame
        Dataframe with accuracy for each motif subset
    
    '''
    dict_df = {'Dataset':[], 'Seed':[], 'Motifs':[], 'Size':[], 'w':[], 'wo':[]}
    for dataset in datasets:
        for size in ['small', 'large']:
            pred_df = pred_df_neg(path, dataset, seeds, size, subexp='w')
            nr_motifs = len(motif_idx.keys())
            for seed in seeds:
                df = pred_df[pred_df['Seed']==seed].reset_index()
                acc_dict = accuracy_motifs(df['w_pred'], df['wo_pred'], df['label'], motif_idx)
                dict_df['Dataset'].extend([dataset]*nr_motifs)
                dict_df['Seed'].extend([seed]*nr_motifs)
                dict_df["Motifs"].extend(acc_dict['nr_motifs'])
                dict_df["Size"].extend([size]*nr_motifs)
                dict_df["w"].extend(acc_dict['w'])
                dict_df["wo"].extend(acc_dict['wo'])
                
    return pd.DataFrame(dict_df)
    
##### REGRESSION #####
def pred_df_regression(path, dataset, seeds, size, file_ending=''):
    ''' Create a dataframe with the predictions and labels for the regression experiments

    Arguments:
    ---------
    path : Path
        Path to the main directory
    dataset : str
        Name of the dataset
    seeds : int[]
        List of seeds
    size : str
        Size of the model (small or large)
    file_ending : str
        File ending of the model

    Output:
    ------
    df : pd.DataFrame
        Dataframe with the predictions and labels for the interaction effect experiments
    '''
    data_path = path / 'Regression' / dataset / 'data' / 'dataset'
    testdata = torch.from_numpy(np.load(data_path / 'data_test.npy')).double()

    dict_df = {'Seed':[], 'add_pred':[], 'enh_pred':[], 'inh_pred':[], 'nonlin_pred':[], 'add_label':[], 'enh_label':[], 'inh_label':[], 'nonlin_label':[]}
    for seed in seeds:
        dict_df['Seed'].extend([seed]*testdata.shape[0])
        for exp in ['add', 'enh', 'inh', 'nonlin']:
            model = load_model(path, 'reg', dataset, exp, size, seed, file_ending)
            model.eval()
            label = np.load(data_path / str('label_test_'+exp+'.npy'))
            with torch.no_grad():
                pred = model(testdata).numpy().flatten().tolist()
            dict_df[exp+'_pred'].extend(pred)
            dict_df[exp+'_label'].extend(label)
    return pd.DataFrame(dict_df)

def calculate_mse(df, exp):
    mse = np.mean((df[exp+'_label']-df[exp+'_pred'])**2)
    return mse

def prediction_to_classification(df, labels):
    '''Numerical errors are difficult to compare due to different scales. Therefore, threshholds between 
    the values were introduced to calculate if value lies near the correct value

    Arguments:
    ----------
    df : pd.DataFrame
        DataFrame containing columns with predictions and labels for each experiment (obtained from pred_df_regression function)
    labels : dict
        Dictionary containing the mapping between the number of motifs and the classification labels
    '''
    acc_df = df.copy()
    thresholds = {}        
    for exp in ['add', 'enh', 'inh', 'nonlin']:
        uniq_labels = df[str(exp+'_label')].unique()
        uniq_labels.sort()
        thresholds[exp] = [uniq_labels[i]+(uniq_labels[i+1]-uniq_labels[i])/2 for i in range(len(uniq_labels)-1)]
        bin_pred = np.digitize(df[exp+'_pred'], thresholds[exp]) 
        acc_df[exp+'_pred'] = list(map(labels[exp].get, bin_pred))

    return pd.DataFrame(acc_df)

def mean_accuracy(df, subset_idx=None):
    ''' Calculate the mean accuracy for each experiment and motif subset of the data

    Arguments:
    ----------
    df : pd.DataFrame
        DataFrame containing columns with predictions and labels for each experiment 
        (obtained from prediction_to_classidication function)
    subset_idx : dict
        Dictionary containing the identifier of a subset as the key and a list of indices as values
        If none, calculate overall accuracy

    Output:
    -------
    pd.DataFrame
        DataFrame containing the mean accuracy for each experiment and (optionally) subset
    '''
    seeds = np.unique(df['Seed'])
    if subset_idx is not None:
        acc_df = {'Experiment':[], 'Subset':[], 'Accuracy':[], 'Seed':[]}
        for seed in seeds:
            for key, value in subset_idx.items():
                df_tmp = df[df['Seed']==seed].copy()
                df_tmp = df_tmp.iloc[value]
                for exp in ['add', 'enh', 'inh', 'nonlin']:
                    acc = np.mean(df_tmp[exp+'_pred']==df_tmp[exp+'_label'])
                    acc_df['Accuracy'] = np.append(acc_df['Accuracy'], acc)
                    acc_df['Experiment'] = np.append(acc_df['Experiment'], exp)
                    acc_df['Subset'] = np.append(acc_df['Subset'], key)
                    acc_df['Seed'] = np.append(acc_df['Seed'], seed)
    else:
        acc_df = {'Experiment':[], 'Accuracy':[], 'Seed':[]}
        for seed in seeds:
            df_tmp = df[df['Seed']==seed].copy()
            for exp in ['add', 'enh', 'inh', 'nonlin']:
                acc = np.mean(df_tmp[exp+'_pred']==df_tmp[exp+'_label'])
                acc_df['Accuracy'] = np.append(acc_df['Accuracy'], acc)
                acc_df['Experiment'] = np.append(acc_df['Experiment'], exp)
                acc_df['Seed'] = np.append(acc_df['Seed'], seed)
    return pd.DataFrame(acc_df)

#############################
#### Attribution methods ####
#############################

def motif_labeler(grammar):
    ''' Assigning binary labels to sequence positions based on if a motif is present at a given position.
    
    Arguments:
    ----------
    grammar: ndarray
            Sequence template for one sequence
    
    Output:
    -------
    label: array[]
        Array with labels for each sequence position: 1 = Motif is present at this position, 0 = Random position
    '''
    gt_info  = []
    for j, gs in enumerate(grammar):
        # calculate information of ground truth
        gt_info.append(np.log2(4) - np.sum(-grammar[j]*np.log2(grammar[j]+1e-10),axis=0))

    # set label if information is greater than 0
    gt_info = np.array(gt_info)
    label = np.zeros(gt_info.shape)
    label[gt_info-1e-5 > 0] = 1
    return label


def motif_label_separator(labels):
    ''' Assigning index labels to sequence positions based on the order of the motifs. Random positions = 0, first motif = 1, second motif = 2, ...
    
    Arguments:
    ----------
    labels: array[]
        Array with binary labels for each sequence position: 1 = Motif is present at this position, 0 = Random position
    
    Output:
    -------
    label: array[]
        Array with labels for each sequence position: 0 = Random position, 1 = First motif, 2 = Second motif, ...
    '''
    c = 1
    motif_labels = []
    flag_new_motif = False
    for i in labels:
        if i == 1:
            flag_new_motif = True
            motif_labels.append(c)
        elif i == 0 and flag_new_motif:
            flag_new_motif = False
            c += 1
            motif_labels.append(0)
        else:
            motif_labels.append(0)
    return motif_labels

def motif_position_index(motif_label):
    ''' Position indices of motifs and random positions. List length equals number of motifs + 1 and contains at the given index 
        (or label as described in motif_label_separator function) a list with the respective indices (0 = Random)'''
    sep_motif_labels = []
    for j in np.unique(motif_label):
        sep_motif_labels.append([i for i, x in enumerate(motif_label) if x == j])
    return sep_motif_labels

def get_motif_percentage(grammar):
    ''' Relative size of motifs with respect to sequence lenght'''
    tmp_labels = motif_labeler(grammar)[0]
    return np.count_nonzero(tmp_labels == 1)/len(tmp_labels)


def calculate_motif_auc(attr, x_model, individual_motifs = False, absolute = True):
    ''' Evaluation of position-wise importance scores.

    Arguments:
    ----------
    attr: array[]
        Importance scores calculated by attribution methods. 
    
    Output:
    -------
    label: array[]
        Array with labels for each sequence position as described above
    '''

    pr_score_motifs = []
    for idx, X_contribs in enumerate(attr):
        #For each test data sequence

        X_model = x_model[idx].swapaxes(0,1)

        label = motif_labeler(X_model)
        motif_label = motif_label_separator(label)
        sep_motif_labels = motif_position_index(motif_label)        
        
        # Extract scores for observed bases to have a one-dimensional importance score array
        # Memo: Works only if we have importance score for one base (not hypothetical importance scores)
        
        #contribs = np.sum(X_contribs[m], axis=0)
        if absolute:
            contribs = np.abs(np.sum(X_contribs, axis=0))
        else:
            contribs = np.sum(X_contribs, axis=0)
        # Precision Recall
        precision, recall, thresholds = precision_recall_curve(label, contribs)
        pr_score = auc(recall, precision)

        # Individual motif evaluations (comparison of one motif against random positions)
        if individual_motifs:
            # For each motif
            tmp_pr_score = []
            for i in range(1, len(sep_motif_labels)):
                label_tmp = np.append([0]*len(sep_motif_labels[0]), [1]*len(sep_motif_labels[i]))
                contribs_tmp = np.append(contribs[sep_motif_labels[0]], contribs[sep_motif_labels[i]])

                precision, recall, _ = precision_recall_curve(label_tmp, contribs_tmp)
                tmp_pr_score.append(auc(recall, precision))

            pr_score_motifs.append(tmp_pr_score)

    if individual_motifs:
        return pr_score, pr_score_motifs
    else:
        return pr_score


###########################
#### Motif Evaluations ####
###########################

def motif_subset_ranges(nr_motifs, nr_motif_idx):
    ''' Creates combinations of motifs for labeling of the sequences with regard to number of contained motifs
    with repect to the data generation process.
    
    Arguments:
    ----------
    nr_motifs: int
        number of motifs
    nr_motif_idx: dict
        dictionary with the number of motifs as keys and the indices of the sequences as values
    '''
    # motif_subsets: indices of missing motifs
    motif_idx_list = list(range(1,nr_motifs+1))
    motif_subsets_removed = list(itertools.chain.from_iterable(itertools.combinations(motif_idx_list, r) for r in motif_idx_list))
    motif_subsets=[list(range(1,nr_motifs+1))] # all motifs
    for t in motif_subsets_removed:
        tmp = motif_idx_list.copy()
        for i in t:
            tmp.remove(i)
        motif_subsets.append(tmp)
    
    # Subsets assigned to number of of containing motifs
    motif_subset_dict = {k:[i for i in motif_subsets if len(i)==k] for k in range(1,nr_motifs+1)}
    new_motif_idx = []
    for k in nr_motif_idx:
        if k == nr_motifs or k == 0: 
            new_motif_idx.append(list(nr_motif_idx[k]))
        else:
            tmp_ranges = np.array_split(nr_motif_idx[k], len(motif_subset_dict[nr_motifs-k]))
            for rng in tmp_ranges:
                new_motif_idx.append(list(rng))
    return motif_subsets, new_motif_idx

def auprc_df(attr,grammar, nr_motif_idx, nr_motifs=4, absolute = True):
    """Returns a dataframe with the AUPRC values for each motif subset.
    
    Arguments:
    ----------
    attr: torch.tensor
        attribution scores
    grammar: torch.tensor
        sequence grammar
    nr_motif_idx: dict
        dictionary with the number of motifs as keys and the indices of the sequences as values
    nr_motifs: int
        number of motifs
    absolute: bool
        if True, the absolute value of the attribution scores is taken for the calculation of the AUPRC.
        If not, then negative effects are ignored.

    Output:
    -------
    pd.DataFrame
        Dataframe with the AUPRC values for each subset.
    """
    auprc_dict = {i:[] for i in range(1,nr_motifs+1)}
    auprc_dict['nr_motifs']=[]
    # motif_subsets: subets of motifs contained in the sequences belonging to the sequence indices in new_motif_idx 
    motif_subsets, new_motif_idx = motif_subset_ranges(nr_motifs, nr_motif_idx)
    nr_subsets = len(motif_subsets)
    for i in range(nr_subsets):
        motif_idx = motif_subsets[i]
        #for all sets except the one containing no motifs
        if len(motif_idx)>0:
            nr_seqs = len(new_motif_idx[i])
            idx = list(new_motif_idx[i])
            _, pr_score_motifs = calculate_motif_auc_classification(attr[idx], grammar[idx], individual_motifs = True, absolute=absolute)
            for j in range(len(motif_idx)):
                auprc_dict[motif_idx[j]].extend([item[j] for item in pr_score_motifs])
            for k in range(1,nr_motifs+1):
                if k not in motif_idx:
                    auprc_dict[k].extend([np.nan]*nr_seqs)
            auprc_dict['nr_motifs'].extend([len(motif_idx)]*nr_seqs)
                              
    return pd.DataFrame(auprc_dict)

def box_auprc_neg(aucdf, exp, method, nr_motif = 4):
    '''Creates dataframe for visualization functions
    
    Arguments:
    ----------
    aucdf: pd.DataFrame
        dataframe with the AUPRC values for each motif subset (output of auprc_df)
    exp: str
        experiment name
    method: str
        attribution method name
    nr_motif: int
        number of motifs
    
    Output:
    -------
    pd.DataFrame
        Dataframe for plotting.
    '''
    #auc_df = np.array([np.array(l) for l in aucdf if l.shape[0]==nr_motif])
    auc_df = aucdf[aucdf['nr_motifs']==nr_motif].drop(columns=['nr_motifs'])
    nr_seq = auc_df.shape[0]
    mot = np.array([])
    auc_vals = np.array([])
    for i in auc_df.columns:
        auc_vals = np.append(auc_vals, auc_df.loc[:,i])
        mot = np.append(mot, [i]*nr_seq)
    df = pd.DataFrame({'AUPRC': auc_vals, 'Motif':mot, 'Method': [method]*len(mot), 'Experiment': [exp]*len(mot)})
    return df

def df_auprc_neg(path, method, size, seeds, refseq='N', nr_motifs=4, absolute = True, target = 1, save_file = False):
    # Calculate AUPRC values for averaged contribution scores and create dataframe including both settings (w and wo)
    
    def avg_contribs_neg(path, modelsize, seeds, w_wo, method, refseq, target=1):
        filenames = [str(modelsize+'_seed'+str(s)+'_'+w_wo+'_'+method+'_'+refseq+'_'+str(target)+'.npy') for s in seeds]
        combined_data = np.array([np.load(path / 'contributions' / fname) for fname in filenames])
        attr = np.mean(combined_data, axis=0)
        return attr

    pwm_model_w = np.load(path / 'data' / 'grammar'/ 'seqpwm_test_w.npy')
    nr_motif_idx = {4: range(0,1500), 3: range(1500, 1900), 2: range(1900, 2500), 1: range(2500, 2900), 0: range(2900, 3000)}
   
    attr_w = avg_contribs_neg(path, size, seeds, 'w', method, refseq, target)
    attr_wo = avg_contribs_neg(path, size, seeds, 'wo', method, refseq, target)

    df_w = auprc_df(attr_w, pwm_model_w, nr_motif_idx, 4, absolute = absolute)
    df_w = box_auprc_neg(df_w, exp = 'w', method = method, nr_motif = nr_motifs)

    df_wo = auprc_df(attr_wo, pwm_model_w, nr_motif_idx, 4, absolute = absolute)
    df_wo = box_auprc_neg(df_wo, exp = 'wo', method = method, nr_motif = nr_motifs)
    df = pd.concat([df_w, df_wo])

    if save_file:
        save_path = path / 'contributions' / 'auprc'
        filename = size+'_'+refseq+'_nrmotifs_'+str(nr_motifs)+'_target'+str(target)+'.npy'
        df.to_csv(save_path / filename)
    return df

#DOC
def box_auprc_reg(aucdf, exp, method = None, nr_motif = 4):
    '''Creates dataframe for visualization functions'''
    auc_df = aucdf[aucdf['nr_motifs'].astype(int)==int(nr_motif)].drop(columns=['nr_motifs'])
    nr_seq = auc_df.shape[0]
    mot = np.array([])
    auc_vals = np.array([])
    for i in auc_df.columns:
        auc_vals = np.append(auc_vals, auc_df.loc[:,i])
        mot = np.append(mot, [i]*nr_seq)
    df = pd.DataFrame({'AUPRC': auc_vals, 'Motif':mot, 'Method': [method]*len(mot), 'Experiment': [exp]*len(mot)})
    return df

# For regression 
def correlation_error_auprc(reg_df, auprc_df, seeds, subset_reg_idx, exp, motif_idx, nr_motifs=4, save_path = None):
    ''' Calculate the correlation between the absolute error and AUPRC for each experiment

    Arguments:
    ----------
    reg_df : pd.DataFrame
        DataFrame containing columns with predictions and labels for each experiment (obtained from pred_df_regression function)
    auprc_df : pd.DataFrame
        DataFrame containing the AUPRC for motif detection for each experiment
    seeds : int[]
        List of seeds
    subset_reg_idx : int[]
        List of indices for the subset of the data
    exp : str
        Experiment name
    save_path : str 
        Path to save the plot (optional)
    '''
    auprc_df_tmp = auprc_df[auprc_df['Nr_Motifs']==nr_motifs]
    auprc_df_tmp = auprc_df_tmp[auprc_df_tmp['Experiment'] == exp]
    auprc_df_tmp = auprc_df_tmp[auprc_df_tmp['Motif'] == motif_idx]
    error = np.array([0]*auprc_df_tmp['AUPRC'].shape[0])
    for seed in seeds:
        df_tmp = reg_df[reg_df['Seed']==seed].copy()
        df_tmp = df_tmp.iloc[subset_reg_idx[nr_motifs]]
        error = np.add(error, np.abs(df_tmp[exp+'_pred'].values-df_tmp[exp+'_label'].values))
    error = error/len(seeds)
    plt.scatter(error, auprc_df_tmp['AUPRC'], s=6)
    plt.ylim(-0.05, 1.05)
    plt.xlabel("Absolute Error")
    plt.ylabel("AUPRC")
    if save_path is not None: 
        plt.savefig(save_path)
        plt.clf()
    #plt.show()
