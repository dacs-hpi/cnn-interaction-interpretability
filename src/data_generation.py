import itertools, copy
from pathlib import Path

import numpy as np
import pandas as pd
from pyjaspar import jaspardb


#############################
###### Folder Structure #####
#############################

# RUN THIS TO GENERATE THE DATA STRUCTURE FOR THOSE EXPERIMENTS
def create_data_folder(path):
    ''' Create Folder structure for saving files.'''
    dir_path = Path(path)
    folders = ['contributions', 'models', 'data']
    data_folder = ['grammar', 'dataset', 'motiflabel']
    for p in folders:
        tmp_path = dir_path / p
        tmp_path.mkdir(parents=True, exist_ok=True)
    dir_path = dir_path / 'data'
    for d in data_folder:
        tmp_path = dir_path / d
        tmp_path.mkdir(parents=True, exist_ok=True)

#############################
###### SEQUENCE GRAMMAR #####
#############################

def generate_model(motif_pwms, seq_length, path = None, min_bp_distance = 15):
    ''' Generates grammar PWMs.
    A grammar is here defined as a template for sequence generation containing a specific order
    and set distances between motifs. Motif PWMs (from Jaspar DB) are inserted in random 
    (background) sequences based on the grammar.
    The resulted grammar PWM contains probabilities for each base at each grammar position.
    Grammar length != seq_length --> Grammar starts with first base of first motif and ends with
    last base from last motif (Length depends on the randomly picked motif distances). This enables
    that the grammar can be placed in different positions in a sequences (not only one).
    
    Attributes
    ----------
    motif_pwms : float[]
        list of matrices with the size motif_length x 4 (JASPAR DB PWMs)
    seq_length : int
        length of sequences   
    path : str or None
        if path is given, then the grammar PWMs are saved in the path directory
    min_bp_distance : int
        minimal distance between motifs (default: 15). Should be higher than CNN filter length
        to avoid overlapping pattern learning

    Output
    ------
    pwm : float[]
        PWMs for sequence grammars
    '''
    # Motifs: List of PMWs, seq_length: Length of sequence samples
    
    motifs = list(motif_pwms.keys())
    num_motifs = len(motifs)
    distance = 0
    for i in motifs:
        distance += motif_pwms[i].shape[1]
    remainder = seq_length - distance
    if remainder < 0:
        print('Length of all motifs is larger than given sequence length. Change motif list or sequence length.')
        return None

    # simulate distances between motifs
    sep = np.array([])
    for l in range(1,num_motifs): 
        max_remain = remainder/(num_motifs)
        sep_tmp = np.random.uniform(min_bp_distance, max_remain, size=1).astype(int)
        sep = np.append(sep, sep_tmp)
        remainder = remainder - sep_tmp
    np.random.shuffle(sep)

    # build a PWM for each grammar without beginning and ending
    if num_motifs>1:
        pwm = np.hstack([motif_pwms[motifs[0]], np.ones((4,sep[0].astype(int)))/4])
        for i in range(1,num_motifs-1):
            pwm = np.hstack([pwm, motif_pwms[motifs[i]], np.ones((4,sep[i].astype(int)))/4])
        pwm = np.hstack([pwm, motif_pwms[motifs[num_motifs-1]]])
    else:
        pwm = np.array(motif_pwms[motifs[0]])

    if path is not None:
        np.save(path, pwm)
    
    return pwm


#############################
#### GROUND TRUTH LABELS ####
#############################

# Help functions for motif position labeling. (Not Sequence labeling)
# Necessary for interpretability evaluation.   

def motif_labeler(grammar):
    ''' Binary labeling based on grammar. 0 is assigned to positions with uniformly distributed base probabilities
    (0.25 for each nucleotide), otherwise 1. 

    grammar : float[]
        Grammar matrices with the size grammar_length x 4. Output from 'generate_model()' function
    '''
    gt_info  = []
    for j, gs in enumerate(grammar):
        # calculate information of ground truth
        gt_info.append(np.log2(4) - np.sum(-grammar[j]*np.log2(grammar[j]+1e-10),axis=0))

    # set label if information is greater than 0
    gt_info = np.array(gt_info)
    label = np.zeros(gt_info.shape)
    label[gt_info-1e-5 > 0] = 1
    return label

def motif_label_separator(labels):
    '''Separate between motifs. Assign individual motif labels to each motif position based
    on the binary labels. 
    '''
    c = 1
    motif_labels = []
    flag_new_motif = False
    for i in labels:
        if i == 1:
            flag_new_motif = True
            motif_labels.append(c)
        elif i == 0 and flag_new_motif:
            flag_new_motif = False
            c += 1
            motif_labels.append(0)
        else:
            motif_labels.append(0)
    return motif_labels

def motif_position_index(motif_label):
    ''' Position indices of motifs and random positions (0 = Random)
    Returns a list for each motif which contains the respective positions indices.
    '''
    sep_motif_labels = []
    for j in np.unique(motif_label):
        sep_motif_labels.append([i for i, x in enumerate(motif_label) if x == j])
    return sep_motif_labels

def get_motif_percentage(grammar):
    '''Calculates which percantage of a sequence belongs to motif positions.
    '''
    tmp_labels = motif_labeler(grammar.swapaxes(0,1))[0]
    return np.count_nonzero(tmp_labels == 1)/len(tmp_labels)

#######################################
### Sequence and Motif Manipulation ###
#######################################

# Help functions to edit motifs in a grammar/sequence.

def replace_motif(pwm, idx, random_pos = True):
    '''Replace all grammar positions given by idx with the average base distribution in this region.
    This equals replacing the motif with a random sequence (and therefore deleting it) but still
    keeping the GC content from the original sequence.
    '''

    if random_pos:
        probs_array = np.ones((4,len(idx)))/4

    else:
        probs = np.sum(pwm[:,idx], axis = 1)
        probs_array = np.array([probs]*len(idx)).T

    new_pwm = np.array(copy.deepcopy(pwm))
    new_pwm[:, idx] = probs_array
    return new_pwm


def delete_grammar_motif(grammar_pwm):
    ''' Takes grammar as an input and deletes subsets of motifs to generate subset motif grammars
    Therefore distances between different motifs are kept the same except that single motifs are missing
    Deleted motifs are replaced with a random sequence. To keep the proportion of nucleotides somehow
    the same, the probabilites for each nucleotide are averaged.

    Arguments
    ---------
    grammar_pwm : float[]
        PWM for grammar

    Output
    -------
    grammars : float[]
        Grammar PWMs. One for each possible motif subset (respectively to motif sets)
    motif_sets : int[]
        indices of motifs in the given subset
    '''

    binary_labels = motif_labeler(grammar_pwm.swapaxes(0,1))
    motif_labels = motif_label_separator(binary_labels)
    uniq_motifs = np.delete(np.unique(motif_labels), 0)
    label_idx = motif_position_index(motif_labels)
    motif_subsets = list(itertools.chain.from_iterable(itertools.combinations(uniq_motifs, r) for r in range(1,len(uniq_motifs)+1)))
    grammars = [grammar_pwm]
    all_motif_set = set(uniq_motifs)
    motif_sets = [uniq_motifs.tolist()]
    for subset in motif_subsets:
        tmp_pwm = copy.deepcopy(grammar_pwm)
        if subset:
        # If subset is empty then all motifs are kept and tmp_pwm = grammar_pwm
            for j in subset:
                tmp_pwm = replace_motif(tmp_pwm, label_idx[j])
        grammars.append(tmp_pwm)
        motif_sets.append(list(all_motif_set.symmetric_difference(set(subset))))
    
    return np.array(grammars), motif_sets



#############################
#### SEQUENCE GENERATION ####
#############################

def simulate_sequence(pwm=None, seq_length=250):
    """Simulate a sequence given a sequence grammar.

    Arguments:
    ----------
    pwm : float[]
        Sequence grammar template (if None then completely random). 
    seq_length : int
        Length of the simulated sequences. Must be longer than pwm.   
    
    Output:
    -------
    one_hot_seq : int[]
        One-hot-encoded sequence
    sequence_pwm : float[]
        Whole sequence template which equals grammar PWM + random template in the begining and
        end so that the template has pre-defined length. 
    """
    if pwm is not None:
        # Fill up sequence in the start and end to get a sequence with the given sequence length
        try:
            remainder = seq_length-pwm.shape[1]
            # Add beginning and end random sequences
            beg = np.random.uniform(0, remainder, 1)[0].astype(int)
            end = remainder - beg

            sequence_pwm = np.hstack([np.ones((4,beg))/4, pwm])
            sequence_pwm = np.hstack([sequence_pwm, np.ones((4,end))/4])
            # sequence length
            seq_length = sequence_pwm.shape[1]
        except IndexError:
            print('Sequence length seq_length must be higher than pwm length.')
    else:
        sequence_pwm =np.array(np.ones((4,seq_length))/4)

    # generate uniform random number for each nucleotide in sequence
    Z = np.random.uniform(0,1,seq_length)

    # calculate cumulative sum of the probabilities
    cum_prob = sequence_pwm.cumsum(axis=0)

    # go through sequence and find bin where random number falls in cumulative 
    # probabilities for each nucleotide
    one_hot_seq = np.zeros((4, seq_length))
    for i in range(seq_length):
        index=[j for j in range(4) if Z[i] < cum_prob[j,i]][0]
        one_hot_seq[index,i] = 1
    return one_hot_seq, sequence_pwm


def generate_one_dataset(pwm = None, seq_length=250, nr_sequences = 20000, path = None, filename = None):
    """Simulate a sequence given a sequence grammar.
    TO DO:  ADJUST PATH AND ID SAVE
            Reduce seq_pwms --> since template is available it is enough to save length of
            start and end subsequence
            
    Arguments:
    ----------
    pwms : float[]
        Sequence grammar template (if None then completely random).
    seq_length : int
        Length of the simulated sequences. Must be longer than pwm.
    nr_sequences : int or int[] (only if motif_sets is not None)
        Number of sequences. If multiple PWM templates (pwm), a list with the number of sequences 
        for each motif_set can be provided. Otherwise, the number will be evenly distributed betwees
        all motif sets.
    
    Output:
    -------
    dataset : int[]
        One-hot-encoded sequences 
    """
    dataset = []
    seq_pwms = []
    for _ in range(nr_sequences):
        tmp_data, tmp_pwm = simulate_sequence(pwm, seq_length)
        dataset.append(tmp_data)
        seq_pwms.append(tmp_pwm)
    return np.array(dataset), np.array(seq_pwms)

def generate_dataset(pwm, number_seqs, seq_length=250):
    """Simulate data sets based on grammar PWMs.

    Arguments:
    ----------
    pwm : float[]
        List of multiple grammar PWMs.
    number_seqs : int[]
        Number of instances for each motif set. Length must equal the number of PWMs.
    seq_length : int
        Length of the simulated sequences.
    
    Output:
    -------
    dataset : int[]
        One-hot-encoded sequences.
    seq_pwms : float[] 
        Underlying template for each sequence. 
    """
    if isinstance(number_seqs, int):
        nr_seqs = [number_seqs]*len(pwm)
    else:
        nr_seqs = number_seqs

    dataset = np.empty([0, 4, seq_length])
    seq_pwms = np.empty([0, 4, seq_length])
    for i in range(pwm.shape[0]):
        tmp_data, tmp_pwm = generate_one_dataset(pwm = pwm[i], seq_length = seq_length, nr_sequences=nr_seqs[i])
        dataset = np.concatenate([dataset, tmp_data], axis=0)
        seq_pwms = np.concatenate([seq_pwms, tmp_pwm], axis=0)
    
    return dataset, seq_pwms

def data_neg_seqs(motif_set, seq_length=250, data_set_size=[2000,500,500], path = None):
    """Generate data for negative sequences experiment

    Arguments:
    ----------
    motif_set : float[]
        List of multiple grammar PWMs.
    seq_length : int
        Length of the simulated sequences.
    data_set_size = int[]
        Number of sequences for training, validation and test set.
    path : str
        Path to save files. If None, the function returns the data.
    
    Output:
    -------
    pwm : float[]
        Template for all sequences with motifs and fix distances. 
    
    grammars : float[]
        Grammar PWMs for all permutations.
    
    data_w : int[]
        One-hot-encoded sequences.
    
    pwms_w : float[]
        Underlying template for each sequence.
    
    label : int[]
        Target values for each sequence. 1 if it contains all motifs, 0 otherwise.
    """
    pwm = generate_model(motif_set, seq_length)

    # Create grammars for each motif subset.
    # grammars[0] contains all motifs, grammars[n-1] contains 0 (random)
    grammars, motif_subsets = delete_grammar_motif(pwm)
    data_set_size_full = [i * (len(motif_subsets)-1) for i in data_set_size]

    if path:
        save_files(pwm, path, 'template', 'grammar')
        save_files(grammars, path, 'subsets', 'grammar')

    # pos: positive class, all motifs; neg_wo: negative class without
    # any motifs; neg_w: Negative class containing 0 to n-1 motifs each sequence
    data_sets = ['train', 'val', 'test']
    for i, data_set in enumerate(data_sets):
        pos_data, pos_pwms = generate_dataset(np.expand_dims(grammars[0], axis=0), data_set_size_full[i])
        neg_wo_data, neg_wo_pwms = generate_dataset(np.expand_dims(grammars[(len(motif_subsets)-1)], axis=0), data_set_size_full[i])
        neg_w_data, neg_w_pwms = generate_dataset(grammars[1:], data_set_size[i])

        data_wo = np.concatenate((pos_data, neg_wo_data), axis=0)
        pwms_wo = np.concatenate((pos_pwms, neg_wo_pwms), axis=0)
        
        data_w = np.concatenate((pos_data, neg_w_data), axis=0)
        pwms_w = np.concatenate((pos_pwms, neg_w_pwms), axis=0)
        
        label = [1]*pos_data.shape[0]+[0]*neg_w_data.shape[0]

        if path:
            save_files(data_wo, path, data_set+'_wo','data')
            save_files(pwms_wo, path, data_set+'_wo','seqpwm')
            save_files(data_w, path, data_set+'_w','data')
            save_files(pwms_w, path, data_set+'_w','seqpwm')            
            save_files(label, path, data_set,'label')

        else:
            return (pwm, grammars, data_w, pwms_w, label)


def data_interaction_effects(motif_set, seq_length=250, data_set_size=[2000,500,500], main_effect=1, enh_inh_const = 2, path = None):
    """Generate data for negative sequences experiment

    Arguments:
    ----------
    motif_set : float[]
        List of multiple grammar PWMs.
    seq_length : int
        Length of the simulated sequences.
    data_set_size = int[]
        Number of sequences for training, validation and test set.
    main_effect : int
        Main effect of motifs. Default = 1.
    enh_inh_const : int
        Constant for enhancing or inhibiting interaction. Default = 2.
    path : str
        Path to save files. If None, the function returns the data.
    
    Output:
    -------
    pwm : float[]
        Template for all sequences with motifs and fix distances. 
    
    grammars : float[]
        Grammar PWMs for all permutations.
    
    data_w : int[]
        One-hot-encoded sequences.
    
    pwms_w : float[]
        Underlying template for each sequence.
    
    labels: int[]
        Target values for each sequence.
    """
    pwm = generate_model(motif_set, seq_length)

    # Create grammars for each motif subset.
    # grammars[0] contains all motifs, grammars[n-1] contains 0 (random)
    grammars, motif_subsets = delete_grammar_motif(pwm)

    if path:
        save_files(pwm, path, 'template', 'grammar')
        save_files(grammars, path, 'subsets', 'grammar')

    data_sets = ['train', 'val', 'test']

    for i, data_set in enumerate(data_sets):
        
        data, pwms = generate_dataset(grammars, data_set_size[i])

        # Additive Interactions: nr_motifs*main_effect
        labels_add = i*[4*main_effect]+4*i*[3*main_effect]+6*i*[2*main_effect]+4*i*[main_effect]+i*[0]
        # Enhancing interaction with a constant: nr_motifs*main_effect (+ constant if all motifs are present)
        labels_enh = i*[4*main_effect+enh_inh_const]+4*i*[3*main_effect]+6*i*[2*main_effect]+4*i*[main_effect]+i*[0]
        # Inhibiting interaction with a constant: nr_motifs*main_effect (- constant if all motifs are present)
        labels_inh = i*[4*main_effect-enh_inh_const]+4*i*[3*main_effect]+6*i*[2*main_effect]+4*i*[main_effect]+i*[0]
        # Non-linear (quadratic) interaction: (nr_motifs*main_effect)^2 / 4
        labels_nonlin = i*[(4*main_effect)**2/4] + 4*i*[(3*main_effect)**2/4] + 6*i*[(2*main_effect)**2/4] + 4*i*[main_effect**2/4] + i*[0]

        if path:
            save_files(data, path, data_set+'_wo','data')
            save_files(pwms, path, data_set+'_wo','seqpwm')
            save_files(labels_add, path, data_set+'_add', 'label')
            save_files(labels_enh, path, data_set+'_enh', 'label')
            save_files(labels_inh, path, data_set+'_inh', 'label')
            save_files(labels_nonlin, path, data_set+'_nonlin', 'label')       
            

        else:
            return (pwm, grammars, data, labels_add, labels_enh, labels_inh, labels_nonlin)
        
#############################
##### REFERENCE SEQUENCES ###
#############################

def get_reference_seqs(ref_mode='N', len_reads=250, gc_content = 0.5, path = None, num_refseq = 5):
        '''Create reference sequences
        
        Arguments:
        ----------
        ref_mode : str
            "N": All zero reference, "GC": Multiple random references with same GC content.
        len_reads : int
            Length of reference reads. Need to have same length like test sequences. Default=250
        gc_content : float
            GC content of sequences. Only when ref_mode = 'GC'. Default = 0.5.
        num_refseq : int
            (optional) Number of reference sequences if ref_mode = 'GC'. Default = 5.
        path : str
            (optional) Path to save reference sequences. Not necessary for ref_mode = 'N'

        Output:
        -------
        ref_samples : float[]
            One-hot encoded reference sequences.
        '''
        # generate reference sequence with N's
        if ref_mode == "N":
            ref_samples = np.zeros((1, len_reads, 4))
    
        # create reference sequences with same GC content as the training data set
        elif ref_mode == "GC":
            ref_samples = np.zeros((num_refseq, len_reads, 4))
            gc_temp = (1-gc_content)/2
            probs = [gc_temp , gc_content/2, gc_content/2, gc_temp]
            # generate reference seqs
            for i in range(num_refseq):
                tmp = np.random.choice([0, 1, 2, 3], p=probs, size=len_reads, replace=True)
                ref_samples[i] = np.eye(4)[tmp]
        
        if path is not None:
            np.save(path, ref_samples)
    
        return ref_samples


#############################
###### MOTIF SIMILARITY #####
#############################

def pearson_motif_correlation(motif_pwms, max_offset=None, col_names = None, motif_pwm_2= None):
    ''' Calculates pairwise similarity between motifs. Pearsons Correlation Coefficient is used.
        Motifs are shifted against each other and the maximal correlation value of the overlapping
        windows is returned. To avoid high correlation of motif tails, a maximal offset can be set. 
        Implementation is based on the dist_pearson function from bio.motifs from the biopython package
        and adjusted for our purpose.
        (biopython/Bio/motifs/matrix.py)

    Arguments
    ---------
    motif_pwms : Bio.Motifs[]
        List of motifs containing PWMs.
    max_offset : int
        Maximal motif shift to calculate correlation
    col_names : list
        If not none, output correlation matrix is returned as pandas dataframe with indices set in col_names
    motif_pwm_2 : float[]
        List of PWMs, used for comparison of first layer filters and motif PWMs

    Output
    -------
    pearson_corr : float[]
        Pairwise similarities for given motifs. Maximal correlation value is 1 (completely similar motifs)
    pearson_offset : int[]
        Offset for maximal correlation value given in pearson_corr
    '''

    if motif_pwm_2 is None:
        motif_pwms2 = copy.deepcopy(motif_pwms)
    else: 
        motif_pwms2 = motif_pwm_2
    m = len(motif_pwms)
    m2 = len(motif_pwms2)

    pearson_corr = np.empty((m,m2))
    pearson_offset = np.empty((m,m2))

    def remove_inf(pwm):
        pwm_tmp = copy.deepcopy(pwm)
        for i in pwm_tmp:
            for j in range(len(pwm_tmp[i])):
                if pwm_tmp[i][j]==0:
                    lst = list(pwm_tmp[i])
                    lst[j]=0.0001
                    pwm_tmp[i]=tuple(lst)
        return pwm_tmp

    
        
    if max_offset is not None:
        offsets = range(-max_offset, max_offset+1)
        for i in range(m):
            pssm_tmp = remove_inf(motif_pwms[i].pwm).log_odds()
            for j in range(m2):
                if motif_pwm_2 is None:
                    pssm_tmp2 = remove_inf(motif_pwms2[j].pwm).log_odds()
                else: 
                    pssm_tmp2 = motif_pwms2[j]
                max_p = -2
                for offset in offsets:
                    if offset < 0:
                        p = pssm_tmp.dist_pearson_at(pssm_tmp2, -offset)
                    else:  # offset>=0
                        p = pssm_tmp2.dist_pearson_at(pssm_tmp, offset)
                    if max_p < p:
                        max_p = p
                        max_o = -offset
                pearson_corr[i][j] = max_p
                pearson_offset[i][j] = max_o
    else:
        for i in range(m):
            pssm_tmp = remove_inf(motif_pwms[i].pwm).log_odds()
            for j in range(m2):
                if motif_pwm_2 is None:
                    pssm_tmp2 = remove_inf(motif_pwms2[j].pwm).log_odds()
                else: 
                    pssm_tmp2 = motif_pwms2[j]
                pearson_corr[i][j] = pssm_tmp.dist_pearson(pssm_tmp2)[0]
                pearson_offset[i][j] = pssm_tmp.dist_pearson(pssm_tmp2)[1]
        pearson_corr = 1-pearson_corr
    
    return pd.DataFrame(pearson_corr, columns=col_names, index=col_names), pearson_offset
    

#############################
####### DATA HANDLING #######
#############################

def save_files(x, path, filename, file_type):
    ''' Save files in predefined folders.'''
    # Types
    # -----
    # 'grammar': Grammar PWM. 1 x 4 x GrammarLength
    # 'seqpwm': Template PWMs for each sequence. NrSeqs x 4 x SeqLength
    # 'data': One-hot-encoded data. NrSeqs x 4 x SeqLength 
    # 'label': Target value or label for each sequence. NrSeqs x 1
    # 'motiflabel_binary': Label for each position if it belongs to a motif (1) or not (0). NrSeqs x SeqLength x 1
    # 'motiflabel_individual':  Index label for each position. If 0, then it is a random sequence position. 
    #                           Otherwise it has the Motif Idx. NrSeqs x SeqLength x 1
    
    save_path = Path(path)
    path_dict = {'grammar': Path('data')/'grammar', 
        'seqpwm': Path('data')/'grammar',
        'data': Path('data')/'dataset', 
        'label': Path('data')/'dataset', 
        'motiflabel_binary': Path('data')/'motiflabel', 
        'motiflabel_individual': Path('data')/'motiflabel', 
        'contribs': Path('contributions')}
    file_name = file_type + '_' + filename + '.npy'
    try:
        savepath = save_path / path_dict[file_type] / file_name
    except NameError:
        print('Wrong file type. Look in Documentation for possible file types')
    np.save(savepath, x)

#############################
###### JASPAR DATABASE ######
#############################
# Normalize JASPAR motif record and create PMW matrix as numpy ndarrays
def norm_array(motif):
    norm_motif_pmw = motif.counts.normalize().values()
    pmw = []
    for i in norm_motif_pmw:
        pmw.append(list(i))
    return np.array(pmw)

# Fetch JASPAR motifs based on ID or Name
# Normalize PMW (such that base probabilities sum up to 1) and return numpy ndarray
def fetch_motifs(motiflist, id_or_name = 'name', norm_motif = True, release_version = 'JASPAR2024'):

    #Connect the JASPAR2022 release object
    jdb_obj = jaspardb(release=release_version)

    if id_or_name == 'id':
        #Fetch motif by ID
        motifs = []
        for m in motiflist:
            motifs.append(jdb_obj.fetch_motif_by_id(m))
    elif id_or_name == 'name':
        #Fetch motifs by TF name
        motifs = jdb_obj.fetch_motifs_by_name(motiflist)
    else: 
        print('Wrong value for id_or_name!')
        return None

    if norm_motif:
        norm_motifs = {}
        for i in motifs:
            if id_or_name == 'id':
                norm_motifs[i.matrix_id] = norm_array(i)
            if id_or_name == 'name':
                norm_motifs[i.name] = norm_array(i)
        return norm_motifs
    else:
        return motifs