import numpy as np
import pandas as pd
from pathlib import Path

import matplotlib.pyplot as plt
from matplotlib import use
import seaborn as sns
import logomaker
from matplotlib.backends.backend_pdf import PdfPages

import sys
sys.path.append('/hpi/fs00/home/marta.lemanczyk/cnn-interaction-interpretability/src')
from evaluation_functions import *

def box_auprc(aucdf, method = None, nr_motif = 4):
    '''Creates dataframe for visualization functions
    
    Arguments:
    ----------
    aucdf: array[]
        Array with AUPRC values for each sequence position
    method: str
        Used attribution method (for plotting purposes)
    
    Output:
    -------
    df: pd.DataFrame
        Dataframe with AUPRC values, motif index and (optional) method
    '''
    auc_df = np.array([np.array(l) for l in aucdf if l.shape[0]==nr_motif])
    nr_seq = auc_df.shape[0]
    nr_motifs = auc_df.shape[1]
    mot = np.array([])
    auc_vals = np.array([])
    for i in range(nr_motifs):
        auc_vals = np.append(auc_vals, auc_df[:,i])
        mot = np.append(mot, [i]*nr_seq)
    if method is not None:
        df = pd.DataFrame({'AUPRC': auc_vals, 'Motif':mot, 'Method': [method]*len(mot)})
    else:
        df = pd.DataFrame({'AUPRC': auc_vals, 'Motif':mot})
    return df

def plot_motif_attr(df, motif_labels, save_path = None):
    ''' Visualization function of AUPRC dataframes '''
    plt.figure(figsize=(8, 6))
    color_pal = sns.set_palette("colorblind")
    sns.set_context("poster")
    with sns.axes_style("darkgrid"):
        ax = sns.boxplot(x="Motif", y="AUPRC", hue="Method",data=df) #, color=colors)
        plt.xticks(rotation = 45, ha = 'right')
        if motif_labels is not None:
            ax.set_xticklabels(motif_labels)
        plt.legend(bbox_to_anchor=(1.01, 1),
            borderaxespad=0)
        if save_path:
            ax.figure.savefig(save_path,bbox_inches='tight', format = 'svg') 
        plt.show()

def plot_single_motif(df, motif_id, save_path = None, title=''):
    ''' Visualization function of AUPRC dataframes for single motifs'''

    plot_df = df[df['Motif']==motif_id]
    plot_df = plot_df.drop(columns=['Motif'])
    
    sns.set_context("poster")
    g = sns.catplot(x="Experiment", y="AUPRC", col="Nr_Motifs", hue="Method", data = plot_df, kind = 'box', legend=False)
    g.set(xlabel = '')
    
    #g.fig.subplots_adjust(top=0.95)
    g.figure.suptitle(title)
    #plt.figure(figsize=(8, 6))
    color_pal = sns.set_palette("colorblind")
    #hue_labels = ['Integrated Gradients', 'Feature Permutation', 'DeepLIFT']
    #g.add_legend(legend_data={
    #    key: value for key, value in zip(hue_labels, g._legend_data.values())
    #})
    g.figure.tight_layout()
    if save_path:
        g.figure.savefig(save_path,bbox_inches='tight', format = 'svg')

def plot_all_positive_motifs(df, save_path = None):
    sns.set_context("poster")
    plot_df = df[df['Nr_Motifs']==4]
    plot_df = plot_df.drop(columns=['Nr_Motifs'])

    g = sns.catplot(x="Method", y="AUPRC", col="Motif", hue="Experiment", data = plot_df, kind = 'box')

    color_pal = sns.set_palette("colorblind")
    if save_path:
        g.figure.savefig(save_path,bbox_inches='tight', format = 'svg')
        
def boxplot_auprc_classification(path, method, size, refseq, nr_motifs=4, idx = None, motif_labels=None, save_path = None):
    '''Boxplots of AUPRC values separating motifs'''
    
    np_w = np.load(path / str(size+'_w_'+method+'_'+refseq+'_'+str(nr_motifs)+'motifs_auprc.npy'), allow_pickle=True)
    np_wo = np.load(path / str(size+'_wo_'+method+'_'+refseq+'_'+str(nr_motifs)+'motifs_auprc.npy'), allow_pickle=True)

    if idx is not None:
        df_w = np.take(np.array([np.array(l) for l in np_w], dtype=object), idx, 0)
        df_wo = np.take(np.array([np.array(l) for l in np_wo], dtype=object), idx, 0)
    else:
        df_w = np.array([np.array(l) for l in np_w])
        df_wo = np.array([np.array(l) for l in np_wo])

    df_w = box_auprc(df_w, method = 'w')
    df_wo = box_auprc(df_wo, method = 'wo')

    df = pd.concat([df_w, df_wo])
    plot_motif_attr(df, motif_labels, save_path = save_path)



def boxplot_auprc_reg(path, methods, size, seeds, refseq='N', nr_motifs=4, absolute = True, target = 1):
    
    def avg_contribs(path, modelsize, seeds, exp, method, refseq, target=1):
    #Average of contributions across all seeds to reduce noise from single models
        filenames = [str(modelsize+'_seed'+str(s)+'_'+exp+'_'+method+'_'+refseq+'_'+str(target)+'.npy') for s in seeds]
        combined_data = np.array([np.load(path / 'contributions' / fname) for fname in filenames])
        attr = np.mean(combined_data, axis=0)
        return attr

    pwm_model = np.load(path / 'data' / 'grammar'/ 'seqpwm_test.npy')
    nr_motif_idx = {4: range(0,500), 3: range(500, 2500), 2: range(2500, 5500), 1: range(5500, 7500), 0: range(7500, 8000)}
   
    df_res = pd.DataFrame()
    for m in methods:
        for exp in ['add', 'enh', 'inh', 'nonlin']:
            attr = avg_contribs(path, size, seeds, exp, m, refseq, target)
            df = auprc_df(attr, pwm_model, nr_motif_idx, nr_motifs, absolute = absolute)
            for i in range(1, nr_motifs+1):
                df_tmp = box_auprc_reg(df, exp, m, nr_motif = i)
                df_tmp['Nr_Motifs']=[i]*df_tmp.shape[0]
                df_res = pd.concat([df_res, df_tmp])
    return df_res


##############
## HEATMAPS ##
##############
def heat_map_contribs(attr, idx = None, colors = "coolwarm", v = None):
    if idx is None:
        att = attr 
    else:
        att = attr[idx]
    sns.set_theme(rc={'figure.figsize':(23.7,1.05)})
    if v is not None:
        ax = sns.heatmap(data = att, vmin=v[0], vmax=v[1], cmap = colors, center = 0, yticklabels=['A', 'C', 'G', 'T'])
    else:
        ax = sns.heatmap(data = att, cmap = colors, center = 0, yticklabels=['A', 'C', 'G', 'T']) 
    plt.show()

def heat_map_grammar(attr, idx = None, save_path = None):
    if idx is None:
        att = attr 
    else:
        att = attr[idx]
    sns.set_theme(rc={'figure.figsize':(23.7,1.05)})
    ax = sns.heatmap(data = att, cmap = "plasma", center = 0, yticklabels=['A', 'C', 'G', 'T'])
    if save_path:
        ax.figure.savefig(save_path,bbox_inches='tight', format = 'svg') 
    plt.show()

##########################
## FILTER VISUALIZATION ##
##########################

# filter sizes small: [8,2], large: [22,0.6]
def plot_filter_logo(filter_mat, neg = False, fig_size=[8,2], fig_path=None, svg = True, title = None, ylim = None):   
    df = pd.DataFrame(np.swapaxes(filter_mat, 0,1)).rename(columns = {0:'A', 1:'C', 2:'G', 3:'T'})
    if not neg:
        df[df<0]=0
    fig, ax = plt.subplots(1,1,figsize=fig_size) 
    if title is not None:
        ax.set_title(title)
    logo_plot = logomaker.Logo(df, shade_below=.5, fade_below=.5, ax=ax)
    if ylim is not None:
        ax.set_ylim(ylim)

    if fig_path:
        use('Agg')
        if svg:
            plt.savefig(fig_path ,format="svg", transparent = True)
        else:
            plt.savefig(fig_path ,format="png", transparent = True)
    #plt.show()
    return logo_plot
      
def min_max_ylim(conv_layer, neg):
    '''Calculates the minimum and maximum values for the y-axis in the filter visualization

    Arguments:
    ----------
    conv_layer: np.array
        Convolutional layer to visualize
    neg: bool
        If True, the negative values are also plotted
    
    Output:
    -------
    ylim: list
        List with minimum and maximum values for the y-axis

    '''
    tmp = conv_layer.copy()
    tmp[tmp<0]=0
    max_val = np.max(np.sum(tmp, axis=1))*1.01

    if neg:
        tmp = conv_layer.copy()
        tmp[tmp>0]=0
        min_val = np.min(np.sum(tmp, axis=1))*1.01
        ylim = [min_val, max_val]
    else:
        ylim = [0, max_val]

    return ylim

def filter_pdf(path, filename, conv_layer, min_thresh=None, neg = False):
    '''Saves pdf with filter visualizations for all filters in the convolutional layer
    
    Arguments:
    ----------
    path: str
        Path to save the pdf
    filename: str
        Name of the pdf file
    conv_layer: np.array
        Convolutional layer to visualize
    min_thresh: float
        Minimum threshold for filter visualization (to remove filters with low activity)
    neg: bool
        If True, the negative values are also plotted
    '''
    
    if neg:
        filename += '_neg'
    pp = PdfPages(Path(path)/str(filename+'.pdf'))
    
    ylim = min_max_ylim(conv_layer, neg)

    for i, filters in enumerate(conv_layer):
        if min_thresh is not None:
            if np.max(filters) > min_thresh:
                f = plot_filter_logo(filters, neg = neg, ylim=ylim).fig
                f.suptitle('Filter '+str(i))
                pp.savefig(f)
                plt.close(f)
        else:
            f = plot_filter_logo(filters, neg = neg, ylim=ylim).fig
            f.suptitle('Filter '+str(i))
            pp.savefig(f)
            plt.close(f)
    pp.close()