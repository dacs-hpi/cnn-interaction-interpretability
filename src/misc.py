import sys
sys.path.append('path/to/cnn-interaction-interpretability/src')

from models.cnn_classification_binary_32 import CNN as bCNN32
from models.cnn_classification_binary_4 import CNN as bCNN4
from models.cnn_regression_32 import CNN as rCNN32
from models.cnn_regression_4 import CNN as rCNN4

def load_model(path, experiment, dataset, subexp, size, seed, fileending=''):
    
    if experiment=='reg':
        model_path = path / 'Regression' 
        if size=='large': 
            CNN = rCNN32
        else: 
            CNN = rCNN4

    elif experiment=='neg':
        model_path = path / 'Classification' / 'negative_sequences'
        if size=='large': 
            CNN = bCNN32
        else: 
            CNN = bCNN4

    else:
        raise Exception("Incorrect experiment type.")

    model_path = model_path / dataset / 'models' / str('model_'+size+'_seed'+str(seed)+'_'+subexp+fileending+'.ckpt')

    model = CNN.load_from_checkpoint(checkpoint_path = model_path).double()
    return model

# Small tables for each dataset and size separately
def create_table(acc_df, dataset, size):
    acc_df_tmp = acc_df[(acc_df['Dataset']==dataset) & (acc_df['Size']==size)]
    seeds = acc_df_tmp['Seed'].unique()
    motifs = acc_df_tmp['Motifs'].unique()
    res_dict = {'Seed': seeds}
    for m in motifs:
        acc_df_motif = acc_df_tmp[acc_df_tmp['Motifs']==m]
        res_dict[str(m)+'_w'] = acc_df_motif['w'].values
        res_dict[str(m)+'_wo'] = acc_df_motif['wo'].values
        
    return pd.DataFrame(res_dict)