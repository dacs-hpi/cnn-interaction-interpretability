# Interpretability
from captum.attr import IntegratedGradients, DeepLiftShap, FeaturePermutation, DeepLift

# Torch Distributed Processing
import torch
from torch import Tensor
from torch.multiprocessing import Process
import torch.distributed as dist
from torch.utils.data.distributed import DistributedSampler
from torch.utils.data import TensorDataset, DataLoader

# Data 
from pathlib import Path
import yaml
import os
import sys
import numpy as np
import argparse

# Own functions
from cnns.cnn_classification_binary_32_old import CNN as bCNN32
from cnns.cnn_classification_binary_4 import CNN as bCNN4
from cnns.cnn_classification_multiclass_32 import CNN as mCNN32
from cnns.cnn_classification_multiclass_8 import CNN as mCNN8
from cnns.cnn_regression_4 import CNN as rCNN4
from cnns.cnn_regression_32 import CNN as rCNN32
from data_generation import get_reference_seqs

# PURPOSE OF SCRIPT #
# This functions are used to analyze the attribution of input features of a CNN.
# For that, we used the Captum framework for interpretability of pytorch models.
# Some methods (eg. Integerated Gradients) require a high amount of RAM. Therefore the calculations 
# were run in a distributed manner using a GPU. 
# See https://captum.ai/tutorials/Distributed_Attribution for further information. 


#########################################################
##### Distributed Calculation of Contribution Scores ####
#########################################################

# Change USE_CUDA to False if Cuda GPU is not available. WORLD_SIZE is minimum of available GPUs. 
if torch.cuda.is_available():
    USE_CUDA = True
else:
    USE_CUDA = False
WORLD_SIZE = 1
    
def run(rank, size, model, testdata, method, target, refseqs, filename):
    ''' Distributed attribution calculation. 
    
    Parameters 
    ----------
    rank : int
        Rank of the process. Set to 1 for single process in the default case.
    size : int
        Number of processes. Set to 1 for single process in the default case.
    model : pytorch model
        Model architecture from cnns folder.
    testdata : pytorch tensor
        Test data for attribution calculation.
    method : str
        Method for attribution calculation. Possible methods are: "ig" (Integrated Gradients), "ds" (DeepSHAP), "fp" (FeaturePermutation), "dl" (DeepLift)
    target : int
        Target class for attribution calculation.
    ref_seqs : ndarray or None
        Reference sequences for attribution calculation. If type is int, zero reference is used with the leNgth of the int.
    '''
    if USE_CUDA:
        total_attr = torch.empty((0,4,testdata.shape[2]), dtype=torch.float64).to('cuda:0')
    else:
        total_attr = torch.empty((0,4,testdata.shape[2]), dtype=torch.float64)

    # Create sampler which divides dataset among processes.
    sampler = DistributedSampler(testdata, num_replicas=size, rank=rank, shuffle=False)
    loader = DataLoader(testdata, batch_size=128, sampler=sampler)
    
    
    if refseqs is None:
        ref_seqs = get_reference_seqs('N', len_reads=testdata.shape[2])
    else:
        ref_seqs = refseqs
    
    # If USE_CUDA, move model to CUDA device with id rank.
    if USE_CUDA:
        model = model.cuda(rank)
        ref_seqs = ref_seqs.to('cuda:0')

    if method == 'ig':
        contrib_method = IntegratedGradients(model)
    elif method == 'ds':
        contrib_method = DeepLiftShap(model)
    elif method == 'fp':
        contrib_method = FeaturePermutation(model)
    elif method == 'dl':
        contrib_method = DeepLift(model)
    else:
        raise ValueError('Please provide a valid method. Possible methods are: "ig" (Integrated Gradients), "ds" (DeepSHAP), "fp" (FeaturePermutation), "dl" (DeepLift)')
    
    for batch in loader:
        #inp = batch[0]
        inp = batch
        if USE_CUDA:
            inp = inp.cuda(rank)
        
        if refseqs is None:
            attr = contrib_method.attribute(inp, target=target)
        else:
            total_attr_temp = torch.empty((0,4,testdata.shape[2]), dtype=torch.float64).to('cuda:0')
            for ref in ref_seqs:
                attr_temp = contrib_method.attribute(inp, baselines=ref, target=target)
                total_attr_temp = torch.cat((total_attr, attr), 0)
            attr = torch.mean(total_attr_temp, dim=0)
        total_attr = torch.cat((total_attr, attr), 0)
    
    contrib_path = Path(str(sys.argv[1])) / 'contributions' / filename
    np.save(contrib_path, total_attr.detach().cpu().numpy())

def init_process(rank, size, fn, model, testdata, method, target=1, ref_seqs=None, backend='gloo'):
    """ Initialize the distributed environment. """
    os.environ['MASTER_ADDR'] = '127.0.0.1'
    os.environ['MASTER_PORT'] = '29500'
    dist.init_process_group(backend, rank=rank, world_size=size)
    fn(rank, size, model, testdata, method, target, ref_seqs)
    dist.destroy_process_group()
 

##############################################
# Run attribution calculations 

parser = argparse.ArgumentParser()
parser.add_argument(
    "--idx", type=int, help="Index for the config file"
)
parser.add_argument(
    "--path", type=str, help="Path to data input/output folder"
)
parser.add_argument(
    "--num_worker", type=int, default=0, help="number of workers for dataloader"
)
args = parser.parse_args()

with open(f"cnn-interaction-interpretability/config/interpret/negative_sequences/config_{args.idx}.yaml", "r") as stream:
    try:
        config = yaml.safe_load(stream)[0]
    except yaml.YAMLError as exc:
        print(exc)  

# Model and Data args
config_exp = config['Model']['config_folder']
g = config['Model']['dataset']
large_model = config['Model']['large_model']
seed=config['Model']['seed']
mconfig = config['Model']['model']

# Interpret args
method = config['Interpret']['method']
target = config['Interpret']['target']
ref_seqs = config['Interpret']['refseqs']

model_name = 'model_'
if large_model:
    model_name = model_name + 'large_seed' + str(seed)
else:
    model_name = model_name + 'small_seed' + str(seed)

# Configurations for different experiments
if config_exp=='negative_sequences':
    path = Path(args.path) / 'Classification'/'negative_sequences'/ g 
    model_path = path / 'models' / str(model_name+'.ckpt')
    testdata_name = 'data_test_w.npy'
    if mconfig=='w':
        m = True
    elif mconfig=='wo': 
        m = False
    model_name += mconfig
    if large_model:
        model = bCNN32().load_from_checkpoint(checkpoint_path = model_path)
    else:
        model = bCNN4().load_from_checkpoint(checkpoint_path = model_path)

elif config_exp=='regression':
    path = Path(args.path) / 'Regression' / g 
    model_path = path / 'models' / str(model_name+'.ckpt')
    if large_model:
        model = rCNN32().load_from_checkpoint(checkpoint_path = model_path)
    else:
        model = rCNN4().load_from_checkpoint(checkpoint_path = model_path)
else:
    # throw exception
    raise Exception("Wrong Experiment Configuration")

# Paths
data_path = path / 'data' 
testdata = torch.from_numpy(np.load(data_path / 'dataset' / testdata_name))
model_path = path / 'models' / str(model_name+'.ckpt')
contrib_filename = model_name.split('model_')[1]+'_'+method+'_'+ref_seqs+'_'+target+'.npy' 
#Reference sequences
if str(ref_seqs)=='GC':
    ref_seqs = torch.from_numpy(np.load(data_path / 'reference_seqs' / 'ref_seq.npy'))
elif str(ref_seqs)=='N':
    ref_seqs = None

size = WORLD_SIZE
processes = []

for rank in range(size):
    p = Process(target=init_process, args=(rank, size, run, model, testdata, method, target, refseqs, contrib_filename))
    p.start()
    processes.append(p)

for p in processes:
    p.join()






