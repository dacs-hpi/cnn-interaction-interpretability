from pathlib import Path
import csv
import os
import sys
import argparse
import yaml
import warnings

from captum.attr import IntegratedGradients, DeepLiftShap, FeaturePermutation, DeepLift

import numpy as np
import torch
from torch import Tensor
from torch.utils.data import TensorDataset, DataLoader

sys.path.append('../cnn-interaction-interpretability')
warnings.filterwarnings("ignore")

from src.models.cnn_classification_binary_32 import CNN as bCNN32
from src.models.cnn_classification_binary_4 import CNN as bCNN4
from src.data_generation import get_reference_seqs

# PURPOSE OF SCRIPT #
# This functions are used to analyze the attribution of input features of a CNN.
# For that, we used the Captum framework for interpretability of pytorch models.
# Some methods (eg. Integerated Gradients) require a high amount of RAM. Therefore the calculations 

def run(model, testdata, method, target, refseqs, num_workers):
    ''' Calculation of contribution scores. 
    
    Parameters 
    ----------
    model : pytorch model
        Model architecture from cnns folder.
    testdata : pytorch tensor
        Test data for attribution calculation.
    method : str
        Method for attribution calculation. Possible methods are: "ig" (Integrated Gradients), "ds" (DeepSHAP), "fp" (FeaturePermutation)
    target : int
        Target class for attribution calculation.
    ref_seqs : ndarray or None
        Reference sequences for attribution calculation. If type is int, zero reference is used with the leNgth of the int.
    filename : str
        Path to save attribution scores.
    '''
    
    def integrated_gradients(model, testdata, ref_seqs=None, target = 1):
        model.eval()
        testdata.requires_grad = True
        contrib_method = IntegratedGradients(model)
        total_attr_temp = torch.empty((testdata.shape[0],4,250), dtype=torch.float64)
        if ref_seqs is None:
            attr = contrib_method.attribute(testdata, target=target)
        else:
            for ref in ref_seqs:
                attr_temp = contrib_method.attribute(testdata, baselines=torch.unsqueeze(ref, 0), target=target)
                total_attr_temp = torch.add(total_attr_temp, attr_temp)
            attr = torch.div(total_attr_temp, ref_seqs.shape[0])
        return attr

    def deepshap(model, testdata, ref_seqs, target = 1):
        model.eval()
        testdata.requires_grad = True
        if refseqs is not None:
            contrib_shap = DeepLiftShap(model)
            attr = contrib_shap.attribute(testdata, baselines=ref_seqs, target=target)
        else:
            contrib_shap = DeepLift(model)
            attr = contrib_shap.attribute(testdata, target=target)
        return attr

    def feature_permutation(model, testdata, target = 1):
        model.eval()
        contrib_method = FeaturePermutation(model)
        attr = contrib_method.attribute(testdata, target=target)
        return attr

    if method == 'ig':
        attr = integrated_gradients(model, testdata, refseqs, target = target)
    elif method == 'ds':
        attr = deepshap(model, testdata, refseqs, target = target)
    elif method == 'fp':
        attr = feature_permutation(model, testdata, target = target)
    else:
        raise ValueError('Please provide a valid method. Possible methods are: "ig" (Integrated Gradients), "ds" (DeepSHAP), "fp" (FeaturePermutation)')

    return attr.detach().numpy()

##############################################
# Run attribution calculations 

parser = argparse.ArgumentParser()
parser.add_argument(
    "--idx", type=int, help="Index for the config file"
)
parser.add_argument(
    "--path", type=str, help="Path to data input/output folder"
)
parser.add_argument(
    "--num_worker", type=int, default=0, help="number of workers for dataloader"
)
args = parser.parse_args()

with open(f"cnn-interaction-interpretability/config/interpret/negative_sequences/config_{args.idx}.yaml", "r") as stream:
    try:
        config = yaml.safe_load(stream)[0]
    except yaml.YAMLError as exc:
        print(exc)  

# Model and Data args
g = config['Model']['dataset']
large_model = config['Model']['large_model']
seed=config['Model']['seed']
mconfig = config['Model']['model']

# Interpret args
method = config['Interpret']['method']
target = config['Interpret']['target']
ref_seqs = config['Interpret']['ref_seqs']

model_name = 'model_'
if large_model:
    model_name = model_name + 'large_seed' + str(seed) + '_'
else:
    model_name = model_name + 'small_seed' + str(seed) + '_'

path = Path(args.path) / 'Classification' / 'negative_sequences' / g
testdata_name = 'data_test_w.npy'
if mconfig=='w':
    m = True
elif mconfig=='wo': 
    m = False

model_name += mconfig
model_path = path / 'models' / str(model_name+'.ckpt')
if large_model:
    model = bCNN32().load_from_checkpoint(checkpoint_path = model_path).double()
else:
    model = bCNN4().load_from_checkpoint(checkpoint_path = model_path).double()

# testdata + reference sequences
datapath = path / 'data' 

testdata = torch.from_numpy(np.load(datapath / 'dataset' / testdata_name))
loader = DataLoader(testdata, batch_size=100, shuffle=False)

if ref_seqs=='GC':
    refseqs = torch.from_numpy(np.load(datapath / 'reference_seqs' / 'ref_seq.npy')).swapaxes(1, 2)
elif ref_seqs=='N':
    #refseqs = torch.from_numpy(get_reference_seqs('N', len_reads=testdata.shape[2])).swapaxes(1, 2)
    #refseqs = torch.unsqueeze(refseqs, 0)
    refseqs = None

# Output filename
contrib_filepath = path / 'contributions' / str(model_name.split('model_')[1]+'_'+method+'_'+ref_seqs+'_'+str(target)+'.npy')
total_attr = np.empty((0,4,testdata.shape[2]))
for batch in loader:
    attr = run(model, batch, method, target, refseqs, contrib_filepath)
    total_attr = np.append(total_attr, attr, axis=0)

np.save(contrib_filepath, total_attr)





