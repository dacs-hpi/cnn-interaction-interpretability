import numpy as np
from pathlib import Path
import argparse
import yaml
import sys
sys.path.append('../cnn-interaction-interpretability')

import torch
import pytorch_lightning as pl
from pytorch_lightning.callbacks import ModelCheckpoint
from pytorch_lightning.loggers import TensorBoardLogger
from cnns.cnn_regression_4 import CNN as smallCNN
from cnns.cnn_regression_32 import CNN as largeCNN


def run_training(path, data_group, seed_nr, interaction, large, epochs, wd, lr, batch_size, num_workers=0):

    # Paths
    model_path = path / data_group / 'models'
    data_path = path / data_group / 'data'/ 'dataset'

    train_data = torch.from_numpy(np.load(data_path / 'data_train.npy').astype(float))
    train_labels = torch.from_numpy(np.load(datapath / ('label_train_'+interaction+'.npy')))

    val_data = torch.from_numpy(np.load(data_path / 'data_val.npy').astype(float))
    val_labels = torch.from_numpy(np.load(datapath / ('label_val_'+interaction+'.npy')))

    if large:    
        filename = 'model_large_seed'+str(seed_nr)+'_'+str(interaction)
    else:
        filename = 'model_small_seed'+str(seed_nr)+'_'+str(interaction)

    # Training
    pl.seed_everything(seed_nr, workers=True)
    if large: 
        model = largeCNN(batch_size, num_workers, wd, lr) 
    else:
        model = smallCNN(batch_size, num_workers, wd, lr) 

    # Create Trainer with checkpoints
    logger = TensorBoardLogger("tb_logs/regression", name=str(data_group + '_' + filename))
    val_checkpoint_callback = ModelCheckpoint(dirpath=model_path, filename = filename, save_top_k = 1, monitor='val_loss')      #filename+'{epoch:02d}'
    progress_bar = pl.callbacks.TQDMProgressBar(refresh_rate=5)
    if torch.cuda.is_available():
        trainer = pl.Trainer(gpus=1, max_epochs=epochs, deterministic=True, callbacks=[val_checkpoint_callback, progress_bar], logger=logger, auto_lr_find=True)
    else:
        trainer = pl.Trainer(gpus=0, max_epochs=epochs, deterministic=True, callbacks=[val_checkpoint_callback, progress_bar], logger=logger, auto_lr_find=True)
    
    # Training and validation data loader
    train_dataloader = model.train_dataloader(train_data, train_labels)
    val_dataloader = model.val_dataloader(val_data, val_labels)
    trainer.fit(model=model, train_dataloaders=train_dataloader, val_dataloaders=val_dataloader)


##############################################
# Run Training 

parser = argparse.ArgumentParser()
parser.add_argument(
    "--idx", type=int, help="Index for the config file"
)
parser.add_argument(
    "--path", type=str, help="Path to data input/output folder"
)
parser.add_argument(
    "--num_worker", type=int, default= 0, help="number of workers for dataloader"
)

args = parser.parse_args()

with open(f"cnn-interaction-interpretability/config/train/regression/config_{args.idx}.yaml", "r") as stream:
    try:
        config = yaml.safe_load(stream)[0]

        # Data args
        g = config['Data']['dataset']
        m = config['Data']['model']

        # Paths
        path = Path(args.path) / 'Regression'
        modelpath = path / g / 'models'
        datapath = path / g / 'data'/ 'dataset'

        # Training args
        seed = config['Train']['seed']
        large_model = config['Train']['large_model']
        lr = config['Train']['learning_rate']
        wd = config['Train']['weight_decay']
        batch_size = config['Train']['batch_size']
        epochs = config['Train']['epochs']

        if args.num_worker > 0:
            run_training(path, data_group=g, seed_nr=seed, interaction=m, large = large_model, epochs=epochs, wd=wd, lr=lr, batch_size=batch_size, num_workers=args.num_worker)
        else:
            run_training(path, data_group=g, seed_nr=seed, interaction=m, large = large_model, epochs=epochs, wd=wd, lr=lr, batch_size=batch_size)

    except yaml.YAMLError as exc:
        print(exc)
 
