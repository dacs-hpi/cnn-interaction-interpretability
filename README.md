# Title of paper
This repository contains code for the preprint "Link".
Data generation and evaluation steps can be found in the respective notebooks. 

## Setup
For model training and interpretability refer to the following description.
### Training
Training configurations can be created in the config_generator.ipynb notebook in the config folder. It automatically generated config files. Please refer to the notebook for further details.
To run model training, run the script with passing over the config file ID (eg. with slurm) as well as the data directory which was generated in the data_generation.ipynb notebook. Note that the functions are adjusted to the folder structure generated in that script. 

```bash
python cnn-interaction-interpretability/training_negative_sequences.py --idx ${SLURM_ARRAY_TASK_ID} --path "/path/to/data"
```

### Interpretation
Similarly to training, run the interpretation script for the experiment of interest.

```bash
python cnn-interaction-interpretability/captum_contribs_negative_sequences.py --idx ${SLURM_ARRAY_TASK_ID} --path "/path/to/data"
```